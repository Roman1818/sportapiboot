<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sport Life</title>
</head>
<body>
<h1 align="center">Sport Life</h1>
<br/>
<table border="3">
    <tr>
        <td>Name of service</td>
        <td>Example of request</td>
    </tr>
    <tr>
        <td>Add user</td>
        <td>http://localhost:8080/add ({"name":"'your name'"})</td>
    </tr>
    <tr>
        <td>Get Sub</td>
        <td>http://localhost:8080/getSub</td>
    </tr>
    <tr>
        <td>Buy</td>
        <td>http://localhost:8080/buy?name='your name'&id='id of subscription'</td>
    </tr>
    <tr>
        <td>Get Time</td>
        <td>http://localhost:8080/getTime?name='your name'</td>
    </tr>
    <tr>
        <td>Enter</td>
        <td>http://localhost:8080/enter?name='your name'</td>
    </tr>
    <tr>
        <td>Exit</td>
        <td>http://localhost:8080/exit?name='your name'</td>
    </tr>

</body>
</html>