create table customer(
     id SERIAL PRIMARY KEY,
     name VARCHAR(50) NOT NULL,
     reg_time DATE NOT NULL
);

create table subscription(
     id SERIAL PRIMARY KEY,
     name VARCHAR(15) NOT NULL,
     time INT8 NOT NULL,
     price INT NOT NULL
);


create table customer_subscription(
      id SERIAL PRIMARY KEY,
      subscription_id INT,
      customer_id INT NOT NULL,
      buy_date DATE,
      status BOOLEAN NOT NULL,
      CONSTRAINT customerSubscription_subscription_id_fk
          FOREIGN KEY (subscription_id) REFERENCES subscription(id),
      CONSTRAINT customerSubscription_customer_id_fk
          FOREIGN KEY (customer_id) REFERENCES customer(id)
);


create table visit(
      id SERIAL PRIMARY KEY,
      start TIMESTAMP NOT NULL,
      finish TIMESTAMP,
      whole_time INT8,
      subscription_id INT NOT NULL,
      CONSTRAINT visit_subscription_id_fk FOREIGN KEY (subscription_id) REFERENCES subscription(id)
);

-- INIT some subscriptions
INSERT INTO subscription(name, time, price)
VALUES ('CLASSIC', 360000, 2000),
       ('GOLD', 720000, 5000),
       ('SILVER',480000, 3000),
       ('CLASSIC', 360000, 2000);



