package com.example.sportapiboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportApiBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportApiBootApplication.class, args);
    }

}
