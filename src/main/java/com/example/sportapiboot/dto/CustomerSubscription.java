package com.example.sportapiboot.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class CustomerSubscription {
    private Integer id;
    private Integer customer_id;
    private Integer subscription_id;
    private LocalDate buyDate;
    private Boolean status;
}
