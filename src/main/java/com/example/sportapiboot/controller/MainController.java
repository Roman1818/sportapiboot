package com.example.sportapiboot.controller;


import com.example.sportapiboot.dto.Customer;
import com.example.sportapiboot.dto.Subscription;
import com.example.sportapiboot.service.SportService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
public class MainController {

    private final SportService sportService;

    @PostMapping("add")
    Customer add(@RequestBody Customer customer) {
        sportService.register(customer.getName());
        return customer;
    }

    @GetMapping("getSub")
    List<Subscription> getSub() {
        return sportService.getSub();


    }

    @GetMapping("buy")
    String buy(@RequestParam String name, Integer id) {
        if (name == null || id == null) return "Enter data!";
        return sportService.buy(name, id);
    }

    @GetMapping("getTime")
    String getTime(@RequestParam String name){
        if(name==null) return "Enter data!";
        return sportService.getTime(name);
    }

    @GetMapping("enter")
    String enter(@RequestParam String name){
        return  sportService.enter(name);
    }

    @GetMapping("exit")
    String exit(@RequestParam String name){
        return sportService.exit(name);
    }


}
